<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Devello;

use ReflectionMethod as PHP_ReflectionMethod;
use Reflection as PHP_Reflection;
use ReflectionClass as PHP_ReflectionClass;


class ReflectionMethod extends PHP_ReflectionMethod {

	public static function getReflection($instanceOrMethodName, $name=null) {
		return new static($instanceOrMethodName, $name);
	}

	public function __construct($instanceOrMethodName, $name = null) {

		if ($name) {
			$reflection = new PHP_ReflectionMethod($instanceOrMethodName, $name);
		} else {
			list ($className, $methodName) = explode("::", $instanceOrMethodName);
			$reflection = new PHP_ReflectionMethod($className, $methodName);
		}
		
		$this->_instance = $reflection;
		
		$selfReflector = new PHP_ReflectionClass($reflection);
		$properties = $selfReflector->getProperties();
		foreach ($properties as $property) {
			if ($property->isStatic() || $property->isPrivate()) {
				continue;
			}
		
			$prop_name = $property->getName();
			$this->$prop_name =&$this->_instance->$prop_name;
		}
	}
	
	public function __toString() {
		return $this->_instance->__toString();
	}
	public function isPublic() {
		return $this->_instance->isPublic();
	}
	public function isPrivate() {
		return $this->_instance->isPrivate();
	}
	public function isProtected() {
		return $this->_instance->isProtected();
	}
	public function isAbstract() {
		return $this->_instance->isAbstract();
	}
	public function isFinal() {
		return $this->_instance->isFinal();
	}
	public function isStatic() {
		return $this->_instance->isStatic();
	}
	public function isConstructor() {
		return $this->_instance->isConstructor();
	}
	public function isDestructor() {
		return $this->_instance->isDestructor();
	}

	public function isMagic() {
		$name = strtolower($this->getName());
		$magicMethods = array(
			'__construct', '__destruct',
			'__call', '__callStatic',
			'__get', '__set',
			'__isset', '__unset',
			'__sleep', '__wakeup',
			'__toString',
			'__invoke',
			'__set_state',
			'__clone'
		);
		
		return in_array($name, $magicMethods);
	}

	public function isEvaled() {
		$parent = $this->getDeclaringClass();
		return $parent->isEvaled();
	}

	public function getModifiers() {
		return $this->_instance->getModifiers();
	}
	public function &invoke($arg0=null, $args=null) {
		$args = func_get_args();
		$ret = &$this->_instance->invokeArgs($arg0, array_slice($args, 1));
		return $ret;
	}

	public function &invokeArgs($arg0, array $arg1) {
		$ret = &$this->_instance->invokeArgs($arg0, $arg1);
		return $ret;
	}

	public function getDeclaringClass() {
		$declaringClass = $this->_instance->getDeclaringClass();
		if ($declaringClass) {
			return ReflectionClass::getReflection($declaringClass->getName());
		}
		return $declaringClass;
	}
	public function getPrototype() {
		return $this->_instance->getPrototype();
	}
	public function setAccessible($arg0) {
		return $this->_instance->setAccessible($arg0);
	}
	public function inNamespace() {
		return $this->_instance->inNamespace();
	}
	public function isClosure() {
		return $this->_instance->isClosure();
	}
	public function isDeprecated() {
		return $this->_instance->isDeprecated();
	}
	public function isInternal() {
		return $this->_instance->isInternal();
	}
	public function isUserDefined() {
		return $this->_instance->isUserDefined();
	}
	
	public function getClosureThis() {
	    return null;
	}
	
	public function getClosureScopeClass() {
		return new ReflectionClass($this->getDeclaringClass());
	}
	
	public function getDocComment() {
		return $this->_instance->getDocComment();
	}
	public function getEndLine() {
		return $this->_instance->getEndLine();
	}
	public function getExtension() {
		return $this->_instance->getExtension();
	}
	public function getExtensionName() {
		return $this->_instance->getExtensionName();
	}
	public function getFileName() {
		$filename = $this->_instance->getFileName();
		if (!file_exists($filename)) {
			$declaringClass = $this->getDeclaringClass();
			return $declaringClass->getFilename();
		}
		return $this->_instance->getFileName();
	}
	public function getName() {
		return $this->_instance->getName();
	}
	public function getNamespaceName() {
		return $this->_instance->getNamespaceName();
	}
	public function getNumberOfParameters() {
		return $this->_instance->getNumberOfParameters();
	}
	public function getNumberOfRequiredParameters() {
		return $this->_instance->getNumberOfRequiredParameters();
	}
	public function getParameters() {
		return $this->_instance->getParameters();
	}
	public function getShortName() {
		return $this->_instance->getShortName();
	}
	public function getStartLine() {
		return $this->_instance->getStartLine();
	}
	public function getStaticVariables() {
		return $this->_instance->getStaticVariables();
	}
	public function returnsReference() {
		return $this->_instance->returnsReference();
	}

	public function getSourceCode() {
		return trim(array_reduce($this->getSourceTokens(), function($carry, $item) {
			if (is_array($item))
				return $carry . $item[1];
			else 
				return $carry . $item;
		}, ''));
	}

	public function getSourceTokens() {
		$tokenizer = Tokenizer::getTokenizer($this);
		$class = $this->getDeclaringClass();
		return $tokenizer->tokens($class->getName() . '::' . $this->getName());

	}

	/**
	 * @param array $override
	 * @return string Signature of the method
	 */
	public function methodSignature($override=array()) {

		if ($this->getDeclaringClass()->isInterface()){
			$override = array_merge($override, array('abstract'=>''));
		}
		$modifiers = $this->_methodModifierNames($override);
		$name = $this->_methodName();
		$parameters = $this->_methodParameters();

		return sprintf('%s function %s(%s)', $modifiers, $name, $parameters);
	}

	private function _methodModifierNames($override=array()) {
		$modifiers = implode(' ', PHP_Reflection::getModifierNames($this->getModifiers()));
		foreach ($override as $from=>$to) {
			$modifiers = str_ireplace($from, $to, $modifiers);
		}
		return $modifiers;
	}

	private function _methodName() {
		$reference = $this->returnsReference() ? '&' : '';
		$name = $this->getName();

		return $reference . $name;
	}

	private function _methodParameters() {
		$parameters = $this->getParameters();
		$paramList = array();
		foreach ($parameters as $parameter) {
			$paramList[] = $this->_methodParameter($parameter);
		}
		$paramList = implode(', ', $paramList);
		return $paramList;
	}

	private static function _methodParameter($parameter) {
		return sprintf('%s%s$%s%s',
			self::_parameterTypehint($parameter),
			$parameter->isPassedByReference() ? '&' : '',
			$parameter->getName(),
			self::_parameterDefaultValue($parameter));
	}

	/**
	 * @param \ReflectionParameter $parameter
	 * @return string
	 */
	private static function _parameterTypehint($parameter) {
		if ($parameter->isArray()) {
			return 'array ';
		}
		if (!$parameter->getClass()) {
			return '';
		}

		$namespace = '\\' . $parameter->getClass()->getNamespaceName();
		return str_replace('\\\\', '\\', $namespace . '\\' . $parameter->getClass()->getShortName() . ' ');
	}

	private static function _parameterDefaultValue($parameter) {
		return ($parameter->isOptional() ? '=' . self::_parameterDefaultAsString($parameter) : '');
	}

	/**
	 * @param \ReflectionParameter $parameter
	 * @return string
	 */
	protected static function _parameterDefaultAsString($parameter) {
		try {
			$value = $parameter->getDefaultValue();
		} catch (\ReflectionException $e) {
			$value = null;
		}

		return var_export($value, true);
	}

	public function getClosure($instance = null) {
		
		$version = explode('.', PHP_VERSION);
		if ($version[0] . $version[1] >= '54') {
		    return $this->_instance->getClosure($instance);
		}
		
		return $this->_createClosure($instance);
	}
	
	private function _createClosure($instance) {
		$params = $this->getParameters();
		array_walk($params, function(&$item){
			$it = '';
			if ($item->isPassedByReference()) {
				$it = '&';
			}
			$it .= '$' . $item->getName();
			$item = $it;
			
		});
		$paramsToPass = $this->getParameters();
		array_walk($paramsToPass, function(&$item) {
			$item = '$' . $item->getName();
		});
		
		$signature = $this->methodSignature(array('static'=>''));
		$signature = str_ireplace('\\$', '$', $signature);
		$signature = preg_replace('/' . $this->getName() . '\\b/', '__invoke', $signature, 1);
		
		$source = sprintf('
			/**
			 * @closureFor %2$s
			 */
			class %1$s extends \\Devello\\Closure {/* extends %2$s */
				/**
				 * @closureFor %4$s
				 */
				%3$s {
					$reflector = new \ReflectionMethod("%2$s::%4$s");
					if (!$reflector->isPublic() || is_null($this->_instance)) {
						$reflector->setAccessible(true);
						$ret = &$reflector->invokeArgs($this->_instance, array(%5$s));
						return $ret;
					}

					$ret = &$this->_instance->%4$s(%6$s);
					return $ret;
				}
			}
			return new %1$s(null);',
			str_ireplace('.', '_', (uniqid('Closure_', true))),
			$this->getDeclaringClass()->getName(),
			$signature,
			$this->getName(),
			implode(', ', $params),
			implode(', ', $paramsToPass)
		);
		
		$closure = eval($source);
		return $closure->bindTo($instance);
	}
}

