<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello;

use stdClass;

class Tokenizer {

	protected static $_cache = array();
	protected static $_fileCache = array();

	protected $_filename;
	
	protected $_observers = array();
	private $_pathStack = array();
	
	public function __construct($filename) {
		$this->_filename = $filename;
		$this->_observers[T_CLASS][] = array($this, 'handleSourceEvent');
		$this->_observers[T_INTERFACE][] = array($this, 'handleSourceEvent');
		$this->_observers[T_FUNCTION][] = array($this, 'handleSourceEvent');
	}
	
	public function handleSourceEvent($event) {
		static::$_cache[$event->name] = array_slice(static::$_fileCache[$this->_filename], $event->start, $event->end - $event->start + 1);
	}
	
	public function scan() {
		$tokens = $this->_tokens();
		$this->_traverse($tokens);
	}
	
	protected function _traverse($tokens, $start=0) {

		$length = count($tokens);
		for ($i = $start, $curlyBracket = 0; $i < $length; ++$i) {
			$token = $tokens[$i];
			if (self::_isOpeningToken($token)) {
				++$curlyBracket;
				continue;
			} elseif ($token === '}') {
				--$curlyBracket;
				if ($curlyBracket < 1) {
					if (count($this->_pathStack) > 1) {
						array_pop($this->_pathStack);
					}
					return $i;
				}
			} elseif ($token === ';' && !$curlyBracket) {
				if (count($this->_pathStack) > 1) {
					array_pop($this->_pathStack);
					return $i;
				}
			}
			if (!is_array($token)) {
				continue;
			}
		
			if ($token[0] == T_NAMESPACE) {
				$namespaceName = $this->_name(array_slice($tokens, $i+1));
				if ($namespaceName) {
					$namespaceName = '\\' . $namespaceName;
				}
				$event = $this->event('namespace ' . $namespaceName, $tokens, $i);
				
				$this->_pathStack[] = $namespaceName;

				$i = $this->_traverse($tokens, $i+1);
				
				$event->end = $i;
				
				array_pop($this->_pathStack);
				
				$this->notifyObservers(T_NAMESPACE, $event);
				
			} elseif ($token[0] == T_CLASS || $token[0] == T_INTERFACE) {
				if (!$this->_pathStack) {
					$this->_pathStack[] = '';
				}
				$className = '\\' . $this->_name(array_slice($tokens, $i+1));

				$event = $this->event('type ' . implode('', array_merge($this->_pathStack, array($className))), $tokens, $i);

				$this->_pathStack[] = $className;
				
				$i = $this->_traverse($tokens, $i+1);

				$event->end = $i;
				if ($token[0] == T_INTERFACE) {
					$this->notifyObservers(T_INTERFACE, $event);
				} else {
					$this->notifyObservers(T_CLASS, $event);
				}
				
			} elseif ($token[0] == T_FUNCTION) {
				if (!$this->_pathStack) {
					$this->_pathStack[] = '';
				}
				$this->_pathStack[] = ((count($this->_pathStack) == 2) ? '::' : '\\'). preg_replace('/\\(.*\\)/s', '', $this->_name(array_slice($tokens, $i+1)));
				
				$event = $this->event(implode('', $this->_pathStack), $tokens, $i);
				$i = $this->_traverse($tokens, $i+1);
				
				$event->end = $i;
				
				$this->notifyObservers(T_FUNCTION, $event);
			}
		}
		return $i;
	}
	private function event($eventName, $tokens, $i) {
		$event = new stdClass();
		$event->name = $eventName;
		$event->start = $i;
		if (true || count($this->_pathStack) == 3) {
			$event->start = $this->eventStart($tokens, $i);
		}
		
		
		return $event;
	}
	private function eventStart($tokens, $i) {
		for($j=$i; $j>=0; --$j) {
			$t = $tokens[$j];
			if ('}' === $t || ';'===$t || '{'===$t || (is_array($t) && $t[0] == T_OPEN_TAG)) {
				return $j + 1;
			}
		}
		return $i;
	}
	
	public function notifyObservers($eventType, $event) {
		if (!empty($this->_observers[$eventType])) {
			foreach ($this->_observers[$eventType] as $observer) {
				if (is_array($observer)) {
					call_user_func($observer, $event);
				} else
					$observer->update($event);
			}
		}
	}
	
	public function on($eventName, $observer) {
		$this->_observers[$eventName][] = $observer;
	}

	/**
	 * @param array $tokens
	 * @param string $namespace
	 * @return array
	 */
	public static function namespace_tokens($namespace, $tokens) {

		$i = static::_scan(T_NAMESPACE, $tokens);
		if (false === $i) {
			return $tokens;
		}
		for ($curlyBracket = 0, $start = $i; $i< count($tokens); ++$i) {
			$token = $tokens[$i];
			if (self::_isOpeningToken($token)) {
				if ($curlyBracket == 0) {
					$name = self::_name(array_slice($tokens, $start+1));
					if ($name != $namespace) {
						$tokens = array_slice($tokens, $i);
						$i = static::_scan(T_NAMESPACE, $tokens);
						$start = $i;
						continue;
					}
				}

				++$curlyBracket;

			} elseif (('}'===$token)) {
				--$curlyBracket;
				if ($curlyBracket < 1) {
					return array_slice($tokens, $start, $i - $start + 1);
				}
			} elseif ($curlyBracket == 0 && (';'===$token)) {
				return $tokens;
			}
		}
		return $tokens;
	}

	public static function interface_tokens($interface, $tokens) {
		$interface = 'type ' . self::normalize_name($interface);
		if (array_key_exists($interface, self::$_cache)) {
			return self::$_cache[$interface];
		}
		list($class, $tokens) = self::_classScope($interface, $tokens);

		return self::_extractTokens($class, $tokens, array(T_INTERFACE, T_CLASS));
	}

	public static function class_tokens($class, $tokens) {
		$class = 'type ' . self::normalize_name($class);
		if (array_key_exists($class, self::$_cache)) {
			return self::$_cache[$class];
		}
	}

	protected static function normalize_name($name) {
		if ($name && $name[0] !== '\\') {
			return '\\' . $name;
		}
		return $name;
	}
	public static function method_tokens($method_full_name, $tokens) {
		$method_full_name = static::normalize_name($method_full_name);

		if (array_key_exists($method_full_name, self::$_cache)) {
			return self::$_cache[$method_full_name];
		}
	}
	
	private static function _methodTokens($tokens, $start, $end) {
		$method_tokens = array_slice($tokens, $start, $end);
		$modifiers = self::_modifiers(array_slice($tokens, 0, $start));
		
		return array_merge($modifiers, $method_tokens);
		
	}
	
	private static function _modifiers($tokens) {
		$modifiers = array();
		foreach(array_reverse($tokens) as $token) {
			if (('}' === $token) || (';' === $token) || ('{'===$token)) {
				break;
			}
			array_unshift($modifiers, $token);
		}
		return $modifiers;
	}
	
	public static function function_tokens($function_full_name, $tokens) {
		$function_full_name = static::normalize_name($function_full_name);
		if (array_key_exists($function_full_name, self::$_cache)) {
			return self::$_cache[$function_full_name];
		}
	}

	private static function _methodFullName($path, $nameTokens) {
		$name = implode("\\", array_filter(explode("\\", $path)))
			. '::'
			. self::_methodName($nameTokens);
		return $name;
	}
	
	private static function _functionFullName($path, $nameTokens) {
		$name = implode("\\", array_filter(array_slice($path, 0, -1))) . '\\' . self::_methodName($nameTokens);
		return $name;
	}

	protected static function _methodName($tokens) {
		foreach ($tokens as $token) {
			if (is_array($token) && $token[0] == T_STRING) {
				return $token[1];
			}
		}
		return uniqid('anonymous_');
	}

	private static function _name($tokens) {
		$length = count($tokens);
		$nameTokens = array();
		for ($j = 0; $j<$length; ++$j) {
			$tok = $tokens[$j];
			if ($tok === '&') {
				continue;
			}
			if ((string) $tok=='(') {
				break;
			}
			
			
			if (is_array($tok) && $tok[0] == T_EXTENDS) {
				break;
			
			}
			if (is_array($tok) && $tok[0] == T_IMPLEMENTS) {
				break;
			}
// 			if ($j && is_array($tok) && $tok[0] == T_WHITESPACE) {
// 				break;
// 			}

			if (self::_isOpeningToken($tok) || $tok === ';') {
			    break;
			}
			
			$nameTokens[] = $tok[1];
		}

		$name = trim(implode("", $nameTokens));
		return $name;
	}
	
	private static function _isOpeningToken($token) {
		return is_array($token) && ($token[0] === T_CURLY_OPEN  || $token[0] === T_DOLLAR_OPEN_CURLY_BRACES) || $token =='{';
	}
	
	private static function _isBlockOpenToken($token) {
		return $token === '{';
	}

	private static function _isClosingToken($token)
	{
		return $token === '}';
	}

	private static function _isSemicolonToken($token) {
		return $token === ';';
	}
	
	/**
	 * 
	 * @param Reflection $reflection
	 * @return Ambigous <\Devello\MethodTokenizer, \Devello\ClassTokenizer, \Devello\FunctionTokenizer>
	 */
	public static function getTokenizer($reflection) {
		if (is_a($reflection, '\\ReflectionMethod')) {
			$class = $reflection->getDeclaringClass();
			$tokenizer = new MethodTokenizer($reflection->getFileName());
		} elseif (is_a($reflection, '\\ReflectionClass')) {
			if (!$reflection->isInterface()) {
				$tokenizer = new ClassTokenizer($reflection->getFileName());
			} else {
				$tokenizer = new InterfaceTokenizer($reflection->getFileName());
			}
		} elseif (is_a($reflection, '\\ReflectionFunction')) {
			$tokenizer = new FunctionTokenizer($reflection->getFilename());
		}
		
		return $tokenizer;
	}
	
	protected function _tokens() {
		if (array_key_exists($this->_filename, static::$_fileCache)) {
			return static::$_fileCache[$this->_filename];
		}
		if (!file_exists($this->_filename)) {
			$file = $this->_virtualizeSourceFile();
		} else {
			$file = file_get_contents($this->_filename);
		}
		
		$tokens = token_get_all($file);
		static::$_fileCache[$this->_filename] = $tokens;
		
		return $tokens;
	}
	
	protected function _virtualizeSourceFile() {
		$code = '<?php
			 class stdClass {
			 }';
		return $code;
	}
}
