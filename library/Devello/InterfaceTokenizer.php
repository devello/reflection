<?php
/*
 Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of Tomasz Cichecki nor the  names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello;

class InterfaceTokenizer extends Tokenizer {
	protected $_interfaceName;
	
	public function tokens($interfaceName) {
		$this->_interfaceName = $interfaceName;
	    $tokens = $this->_tokens();
	    $this->scan();
        return static::interface_tokens($this->_interfaceName, $tokens);
	}
	
	protected function _virtualizeSourceFile() {
		$this->_reflection = new ReflectionClass($this->_interfaceName);
		$namespace = $this->_reflection->getNamespaceName();
		if ($namespace) {
			$namespace = 'namespace ' . $namespace . ';';
		}
		$code = sprintf("<?php
	$namespace
	interface %s%s {
		%s
	}" . PHP_EOL, $this->_reflection->getShortName(), $this->_extends(), $this->_methods());
	
	    return $code;
	}
	
	private function _extends() {
	    $parent = $this->_reflection->getParentClass();
	
	    if (empty($parent)) {
	        return "";
	        	
	    }
	    return " extends " . $parent->getName();
	}
	
	
	private function _methods() {
	    $methods = $this->_reflection->getMethods();
	    array_walk($methods, function(&$item) {
	        $item = $item->methodSignature() . ';';
	    });
	
        return implode(PHP_EOL, $methods);
	}
}