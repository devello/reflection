<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello;

use ReflectionClass as PHP_ReflectionClass;

class ReflectionClass extends PHP_ReflectionClass {

	public static function getReflection($klass) {
		return new static($klass);
	}

	public function __construct($argument = null) {

		$this->_instance = new PHP_ReflectionClass($argument);
		
		
		$selfReflector = new PHP_ReflectionClass($this->_instance);
		
		$properties = $selfReflector->getProperties();
		foreach ($properties as $property) {
			if ($property->isStatic() || $property->isPrivate()) {
				continue;
			}

			$prop_name = $property->getName();
			$this->$prop_name =&$this->_instance->$prop_name;
		}
	}

	public function __toString() {
		return $this->_instance->__toString();
	}
	public function getName() {
		return $this->_instance->getName();
	}
	public function isInternal() {
		return $this->_instance->isInternal();
	}
	public function isUserDefined() {
		return $this->_instance->isUserDefined();
	}
	public function isInstantiable() {
		return $this->_instance->isInstantiable();
	}

	public function isEvaled() {
		if (0 !== stripos($this->getFileName(), 'devello:eval\'d code::/')) {
			return false;
		}
		return true;
	}

	public function getFileName() {
		$filename = $this->_instance->getFileName();
		if (!file_exists($filename)) {
			return 'devello:eval\'d code::/' . $this->getName();
		}
		return $this->_instance->getFileName();
	}
	public function getStartLine() {
		return $this->_instance->getStartLine();
	}
	public function getEndLine() {
		return $this->_instance->getEndLine();
	}
	public function getDocComment() {
		return $this->_instance->getDocComment();
	}
	public function getConstructor() {
		return $this->_instance->getConstructor();
	}
	public function hasMethod($arg0) {
		return $this->_instance->hasMethod($arg0);
	}
	public function getMethod($arg0) {
		$reflection = $this->_instance->getMethod($arg0);
		$fullName = $this->_instance->getName();

		$decorated = new ReflectionMethod($fullName, $reflection->getName());
		return $decorated;
	}
	public function getMethods($arg0=NULL) {
		if (is_null($arg0)) {
			$methods = $this->_instance->getMethods();
		} else {
			$methods = $this->_instance->getMethods($arg0);
		}
		array_walk($methods, function(\ReflectionMethod &$method)  {
			$method = new ReflectionMethod(
				$method->getDeclaringClass()->getName(),
				$method->getName()
			);
		});
		return $methods;
	}

	public function hasProperty($arg0) {
		return $this->_instance->hasProperty($arg0);
	}
	public function getProperty($arg0) {
		return $this->_instance->getProperty($arg0);
	}
	public function getProperties($arg0=NULL) {
		if (is_null($arg0)) {
			return $this->_instance->getProperties();
		} else
			return $this->_instance->getProperties($arg0);
	}
	public function hasConstant($arg0) {
		return $this->_instance->hasConstant($arg0);
	}
	public function getConstants() {
		return $this->_instance->getConstants();
	}
	public function getConstant($arg0) {
		return $this->_instance->getConstant($arg0);
	}
	public function getInterfaces() {
		return $this->_instance->getInterfaces();
	}
	public function getInterfaceNames() {
		return $this->_instance->getInterfaceNames();
	}
	public function isInterface() {
		return $this->_instance->isInterface();
	}
	public function isAbstract() {
		return $this->_instance->isAbstract();
	}
	public function isFinal() {
		return $this->_instance->isFinal();
	}
	public function getModifiers() {
		return $this->_instance->getModifiers();
	}
	public function isInstance($arg0) {
		return $this->_instance->isInstance($arg0);
	}
	public function newInstance($arg0) {
		if (!$this->hasConstructor()) {
			$className = $this->getName();
			return new $className();
		}
		return $this->_instance->newInstance($arg0);
	}
	public function newInstanceArgs(array $arg0=NULL) {
		if (!$this->hasConstructor()) {
			$className = $this->getName();
			return new $className();
		}
		return $this->_instance->newInstanceArgs($arg0);
	}

	public function hasConstructor() {
		if ($this->hasMethod('__construct')) {
			return true;
		}
		if ($this->hasMethod($this->getShortName())) {
			return true;
		}
		$parentClass = $this->getParentClass();
		if ($parentClass) {
			return $parentClass->hasConstructor();
		}
		return false;
	}
	public function getParentClass() {
		$reflector = $this->_instance->getParentClass();
		if (!$reflector) {
			return $reflector;
		}
		return static::getReflection($reflector);

	}
	public function isSubclassOf($arg0) {
		return $this->_instance->isSubclassOf($arg0);
	}
	public function getStaticProperties() {
		return $this->_instance->getStaticProperties();
	}
	public function getStaticPropertyValue($arg0, $arg1=NULL) {
		return $this->_instance->getStaticPropertyValue($arg0, $arg1);
	}
	public function setStaticPropertyValue($arg0, $arg1) {
		return $this->_instance->setStaticPropertyValue($arg0, $arg1);
	}
	public function getDefaultProperties() {
		return $this->_instance->getDefaultProperties();
	}
	public function isIterateable() {
		return $this->_instance->getDefaultProperties();
	}
	public function implementsInterface($arg0) {
		return $this->_instance->implementsInterface($arg0);
	}
	public function getExtension() {
		return $this->_instance->getExtension();
	}
	public function getExtensionName() {
		return $this->_instance->getExtensionName();
	}
	public function inNamespace() {
		return $this->_instance->inNamespace();
	}
	public function getNamespaceName() {
		return $this->_instance->getNamespaceName();
	}
	public function getShortName() {
		return $this->_instance->getShortName();
	}

	public function newInstanceWithoutConstructor() {

		$version = explode('.', PHP_VERSION);
		if ($version[0] . $version[1] < '54') {
			return $this->_newInstanceWithoutConstructor();
		}

		$ret = $this->_instance->newInstanceWithoutConstructor();
		return $ret;
	}
	
	protected function _newInstanceWithoutConstructor() {
		$properties = $this->getProperties();
		$defaults = $this->getDefaultProperties();
			
		$serialized = "O:" . strlen($this->getName());
		$serialized .= ":\"{$this->getName()}\":" . count($properties) . ':{';
			
		foreach ($properties as $property) {
			$name = $property->getName();
			if ($property->isProtected()) {
				$name = chr(0) . '*' . chr(0) . $name;
			} elseif ($property->isPrivate()) {
				$name = chr(0) . $this->getName() . chr(0) . $name;
			}
			$serialized .= serialize($name);
				
			$default = null;
			if (array_key_exists($property->getName(), $defaults)) {
				$default = $defaults[$property->getName()];
			}
			$serialized .= serialize($default);
		}
		$serialized .= "}";
		return unserialize($serialized);
	}

	public function getSourceCode() {
		return trim(array_reduce($this->getSourceTokens(), function($carry, $item) {
			if (is_array($item))
				return $carry . $item[1];
			else 
				return $carry . $item;
		}, ''));
	}

	public function getSourceTokens() {
		$tokenizer = Tokenizer::getTokenizer($this);
		return $tokenizer->tokens($this->getName());
	}

}
