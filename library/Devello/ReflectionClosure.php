<?php
namespace Devello;

class ReflectionClosure extends ReflectionMethod {
	private $_binding = null;
	
	public function __construct($instanceOrMethodName, $name) {
		parent::__construct($instanceOrMethodName, $name);
		$this->_binding = $instanceOrMethodName;
	}
	
	public function getName() {
		$docComment = $this->getDocComment();
		if ($docComment && preg_match('/\\@closureFor\\s(\\S*)\\b/', $docComment, $matches)) {
			return $matches[1];
		}
		return parent::getName();
	}
	
	public function getClosureThis() {
		return Closure::getBoundInstance($this->_binding);
	}
	
	public function getClosureScopeClass() {
		$docComment = $this->getDeclaringClass()->getDocComment();
		if ($docComment && preg_match('/\\@closureFor\\s(\\S*)\\b/', $docComment, $matches)) {
		    return new ReflectionClass($matches[1]);
		}
		return new ReflectionClass($this->getDeclaringClass());
	}
}