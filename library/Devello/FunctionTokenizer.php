<?php
/*
 Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of Tomasz Cichecki nor the  names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello;

class FunctionTokenizer extends Tokenizer {
	protected $_functionName;
	
	public function tokens($functionName) {
		$this->_functionName = $functionName;
		$tokens = $this->_tokens();
		$function_tokens = static::function_tokens($this->_functionName, array());
		if (!$function_tokens)
		    $this->scan();
		return static::function_tokens($this->_functionName, $tokens);
	}
	
	protected function _virtualizeSourceFile() {
		$this->_reflection = new ReflectionFunction($this->_functionName);
		$namespaceName = $this->_reflection->getNamespaceName();
		if ($namespaceName) {
			$namespaceName = sprintf('namespace %s;', $namespaceName);
		} else {
			$namespaceName = '';
		}
		$code = '<?php
			%s
			%s {
				//virtualized evaled code, real source code inaccessible
			}
		';
		
		return sprintf($code, $namespaceName, $this->_reflection->methodSignature());
	}
}
