<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello;

use ReflectionFunction as PHP_ReflectionFunction;
use ReflectionClass as PHP_ReflectionClass;

class ReflectionFunction extends PHP_ReflectionFunction
{
	public function __construct($name = null)
	{
		if (is_object($name) && is_subclass_of($name, '\\Devello\\Closure')) {
			$this->_instance = new ReflectionClosure($name, '__invoke');
		} else
			$this->_instance = new PHP_ReflectionFunction($name);
		
		
		$selfReflector = new PHP_ReflectionClass($this->_instance);
		
		$properties = $selfReflector->getProperties();
		foreach ($properties as $property) {
			if ($property->isStatic() || $property->isPrivate()) {
				continue;
			}

			$prop_name = $property->getName();
			$this->$prop_name =&$this->_instance->$prop_name;
		}
	}
	
	public static function getReflection($functionName) {
	
		return new static($functionName);
	}
	

	public function isDisabled()
	{
		$return_value = $this->_instance->isDisabled();
		return $return_value;
	}

	public function &invoke($args=NULL)
	{
		$return_value = &$this->_instance->invoke($args);
		return $return_value;
	}

	public function &invokeArgs(array $args)
	{
		$return_value = &$this->_instance->invokeArgs($args);
		return $return_value;
	}

	public function inNamespace()
	{
		$return_value = $this->_instance->inNamespace();
		return $return_value;
	}

	public function isClosure()
	{
		$return_value = $this->_instance->isClosure();

		return $return_value;
	}

	public function isDeprecated()
	{
		$return_value = $this->_instance->isDeprecated();
		return $return_value;
	}

	public function isInternal()
	{
		$return_value = $this->_instance->isInternal();
		return $return_value;
	}

	public function isUserDefined()
	{
		$return_value = $this->_instance->isUserDefined();
		return $return_value;
	}

	public function getClosureThis() {
		return $this->_instance->getClosureThis();
	}
	
	public function getClosureScopeClass() {
		return $this->_instance->getClosureScopeClass();
	}
	
	public function getDocComment()
	{
		$return_value = $this->_instance->getDocComment();
		return $return_value;
	}

	public function getEndLine()
	{
		$return_value = $this->_instance->getEndLine();
		return $return_value;
	}

	public function getExtension()
	{
		$return_value = $this->_instance->getExtension();
		return $return_value;
	}

	public function getExtensionName()
	{
		$return_value = $this->_instance->getExtensionName();
		return $return_value;
	}

	public function getFileName()
	{
		$filename = $this->_instance->getFileName();
		if (!file_exists($filename)) {
			return 'devello:eval\'d code::/' . $this->getName();
		}
		return $this->_instance->getFileName();
	}

	public function getName()
	{
		if ($this->isClosure()) {
			return $this->_nameClosure();
		}
		$return_value = $this->_instance->getName();
		return $return_value;
	}
	
	protected function _nameClosure() {
		$name = $this->_instance->getName();
		return str_replace('{closure}', sprintf('{closure[%d:%d]}', $this->getStartLine(), $this->getEndLine()), $name);
	}

	public function getNamespaceName()
	{
		$return_value = $this->_instance->getNamespaceName();
		return $return_value;
	}

	public function getNumberOfParameters()
	{
		$return_value = $this->_instance->getNumberOfParameters();
		return $return_value;
	}

	public function getNumberOfRequiredParameters()
	{
		$return_value = $this->_instance->getNumberOfRequiredParameters();
		return $return_value;
	}

	public function getParameters()
	{
		$return_value = $this->_instance->getParameters();
		return $return_value;
	}

	public function getShortName()
	{
		$return_value = $this->_instance->getShortName();
		return $return_value;
	}

	public function getStartLine()
	{
		$return_value = $this->_instance->getStartLine();
		return $return_value;
	}

	public function getStaticVariables()
	{
		$return_value = $this->_instance->getStaticVariables();
		return $return_value;
	}

	public function returnsReference()
	{
		$return_value = $this->_instance->returnsReference();
		return $return_value;
	}

	public function getSourceCode() {
		if ($this->isClosure()) {
			return $this->_closureCode();
		}
		return $this->getBodySource();
	}
	
	protected function _closureCode() {
		$startLine = $this->getStartLine();
		$endLine = $this->getEndLine();
		
		$source = file($this->getFileName());
			
		$source = array_slice($source, $startLine-1, $endLine - $startLine + 1);
		$source[0] = preg_replace('/.*\\b(function)/', '$1', $source[0], 1);
		return trim(implode("", $source));
	}

	public function getBodySource() {
		return trim(array_reduce($this->getSourceTokens(), function($carry, $item) {
			if (is_array($item))
				return $carry . $item[1];
			else 
				return $carry . $item;
		}, ''));
	}

	public function getSourceTokens() {
		$tokenizer = Tokenizer::getTokenizer($this);
		return $tokenizer->tokens($this->getName());

	}
	
	public function getClosure() {
		
		$version = explode('.', PHP_VERSION);
		if ($version[0] . $version[1] >= '54') {
			return $this->_instance->getClosure();
		}
		
		$parameters = $this->getParameters();
		$argsToPassThrough = array();
		
		foreach ($parameters as $no=>$parameter) {
			$argToPassThrough = '$' . $parameter->getName();
			$argsToPassThrough[]=$argToPassThrough;
		}
		
		$source = sprintf('
			class %1$s extends \\Devello\\Closure {
				/**
				 * @closureFor %4$s
				 */
				public function %2$s__invoke(%3$s) {
					$ret = %2$s%4$s(%5$s);
					return $ret;
				}
				
				private function _getClosuredFunctionName() {
					return "%4$s";
				}
			}
			return new %1$s(null);',
			str_ireplace('.', '_', (uniqid('Closure_', true))),
			($this->returnsReference() ? '&': ''),
			str_ireplace('\\$','$',$this->_methodParameters()),
			$this->getName(),
			implode(', ', $argsToPassThrough)
		);
		return eval($source);
	}
	
	/**
	 * @param array $override
	 * @return string Signature of the method
	 */
	public function methodSignature($override=array()) {
	
		$name = $this->_methodName();
		$parameters = $this->_methodParameters();
	
		return sprintf('function %s(%s)', $name, $parameters);
	}
	
	private function _methodName() {
		$reference = $this->returnsReference() ? '&' : '';
		$name = $this->getShortName();
	
		return $reference . $name;
	}
	
	private function _methodParameters() {
		$parameters = $this->getParameters();
		$paramList = array();
		foreach ($parameters as $parameter) {
			$paramList[] = $this->_methodParameter($parameter);
		}
		$paramList = implode(', ', $paramList);
		return $paramList;
	}
	
	private static function _methodParameter($parameter) {
		return sprintf('%s%s$%s%s',
			self::_parameterTypehint($parameter),
			$parameter->isPassedByReference() ? '&' : '',
			$parameter->getName(),
			self::_parameterDefaultValue($parameter));
	}
	
	/**
	 * @param \ReflectionParameter $parameter
	 * @return string
	 */
	private static function _parameterTypehint($parameter) {
		if ($parameter->isArray()) {
			return 'array ';
		}
		if (!$parameter->getClass()) {
			return '';
		}

		$namespace = '\\' . $parameter->getClass()->getNamespaceName();
		return str_replace('\\\\', '\\', $namespace . '\\' . $parameter->getClass()->getShortName() . ' ');
	}
	
	private static function _parameterDefaultValue($parameter) {
		return ($parameter->isOptional() ? '=' . self::_parameterDefaultAsString($parameter) : '');
	}
	
	/**
	 * @param \ReflectionParameter $parameter
	 * @return string
	 */
	protected static function _parameterDefaultAsString($parameter) {
		try {
			$value = $parameter->getDefaultValue();
		} catch (\ReflectionException $e) {
			$value = null;
		}
	
		return var_export($value, true);
	}
}
