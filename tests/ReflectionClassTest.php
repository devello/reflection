<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello\Reflection\Tests;

use PHPUnit_Framework_TestCase, Devello\ReflectionClass;

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NamespacedExamples.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Examples.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'OverlappingNamespace.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AbstractClass.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'FinalClass2.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NoConstructor.php';

class ReflectionClassTest extends PHPUnit_Framework_TestCase {

	public function testGetSourceCode_WellFormedSourceCode_FullSourceCodeIsRetrieved() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\ExampleEmptyClass');
		$source = trim($reflector->getSourceCode());

		$this->assertEquals('class ExampleEmptyClass {

}', $source);
	}

	public function testGetSourceCode_BadlyFormedSourceCode_SourceCodeIsRetrieved() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\ExampleBadFormatting');
		$source = trim($reflector->getSourceCode());

		$this->assertEquals('class ExampleBadFormatting {}', $source);
	}

	public function testGetSourceCode_BadlyFormedSourceCodeInlineNamespaces_SourceCodeIsRetrived() {
		$reflector = ReflectionClass::getReflection('Devello\Tests\name3\\ExampleInNamespace');
		$source = trim($reflector->getSourceCode());

		$this->assertEquals('class ExampleInNamespace{public function method(){return 0;}}', $source);
	}

	public function testGetSourceCode_FailingExampleNo1_SourceCode() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\ExampleWithProtectedMethods');

		$method = $reflector->getMethod('method_public');
		$source = $method->getSourceCode();

		$this->assertEquals('public function method_public() {
		return $this->method_protected();
	}', trim($source));
	}

	public function testGetSourceCode_FailingExampleNo2_SourceCode() {
		
		$reflector = ReflectionClass::getReflection('\\PHPUnit_Framework_TestCase');

		$methods = $reflector->getMethods();
		
		foreach ($methods as $method) {
			$method->getSourceCode();
		}
	}

	public function testGetSourceCode_GettingInterfaceSourceCode_SourceCodeIsReturned() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\DummyInterface');

		$source = $reflector->getSourceCode();

		$this->assertEquals('interface DummyInterface {

}', trim($source));
	}

	public function testGetSourceCode_GettingDerivedClassSourceCode_SourceCodeIsReturned() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\Example1');

		$source = $reflector->getSourceCode();

		$this->assertEquals('class Example1 extends ExampleBadFormatting {}', trim($source));
	}

	public function testGetSourceCode_GettingClassImplementingInterfaceSourceCode_SourceCodeIsReturned() {
		$reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\Example2');

		$source = $reflector->getSourceCode();

		$this->assertEquals('class Example2 implements DummyInterface {}', trim($source));
	}

	public function testEvaldFilename_CallingGetFilenameOnDifferentEvaldTypes_DifferentNamesAreDefined() {
		$type1 = $this->getMock('stdClass', array('a', 'b'));
		$type2 = $this->getMock('stdClass', array('x', 'y', 'z'));

		$reflection1 = ReflectionClass::getReflection($type1);
		$reflection2 = ReflectionClass::getReflection($type2);

		$this->assertNotEquals($reflection1->getFileName(), $reflection2->getFileName());
	}

	public function testGetSourceCode_GettingEvaldClass_SourceCodeIsReturned() {
		$type = $this->getMock('stdClass', array('a', 'b'));
		$reflector = ReflectionClass::getReflection($type);
		$source = $reflector->getSourceCode();

		$this->assertNotEmpty($source);
	}

	public function testGetSourceCode_GetingEvaldClassInNamespace_SourceCodeIsReturned() {

		eval('namespace x\\y;  class stdClass {public function a(){}}');
		$reflector = ReflectionClass::getReflection('x\\y\\stdClass');
		foreach ($reflector->getMethods() as $method){ 
			$method->getSourceCode();
		}
		$this->assertEquals('stdClass', $reflector->getShortName());
		$this->assertEquals('x\\y', $reflector->getNamespaceName());

	}

	public function testEvaldFilename_CheckingIfSourceIsEvald_ReflectionReturnsTrue() {
		eval('namespace z\\x; class stdClass {}');

		$reflector = ReflectionClass::getReflection('z\\x\\stdClass');

		$isEvaled = $reflector->isEvaled();

		$this->assertTrue($isEvaled);
	}

	public function testGetMethods_NoParameters_AllMethodsAreReturned() {
		$reflection = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\ExampleWithProtectedMethods');
		$methods = $reflection->getMethods();

		$this->assertEquals(4, count($methods));
	}

	public function testGetProperties_NoParameters_AllPropertiesAreReturned() {
		$reflection = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\Example_4');
		$properties = $reflection->getProperties();

		$this->assertEquals(3, count($properties));
	}
	
	public function testGetSourceCode_NamespaceNameOverlapsClassName_SourceCodeIsFetched() {
		$reflection = new ReflectionClass('overlappingExample');
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertEquals('class overlappingExample {}', $sourceCode);
	}
	
	public function testGetSourceCode_AnAbstractClass_SourceCodeIsFetched() {
		$reflection = new ReflectionClass('AbstractClass2');
		
		$sourceCode = $reflection->getSourceCode();
		$this->assertEquals('abstract class AbstractClass2 {

}', $sourceCode);
	}
	
	public function testGetSourceCode_ClassModifierPhpShortOpenTag_SourceModifierIsFetched() {
		$reflection = new ReflectionClass('FinalClass2');
		
		$sourceCode = $reflection->getSourceCode();
		$this->assertEquals('final class FinalClass2 {

}', $sourceCode);
		
	}

	public function testHasConstructor_OldStyleConstructorInBaseClass_ReturnsTrue() {
		$reflection = new ReflectionClass('Devello\\Reflection\\Tests\\EmptyExtendingOldStyleClass');
		$hasConstructor = $reflection->hasConstructor();
		$this->assertTrue($hasConstructor);
	}
//	public function testGetSourceCode_MultipleInlineNamespaces_StartTokenIsReturned() {
////		$reflector = ReflectionClass::getReflection('Devello\Tests\name3\\ExampleInNamespace');
//
////		$this->assertEquals(50, $reflector->getStartToken());
//	}
}


