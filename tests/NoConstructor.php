<?php
namespace Devello\Reflection\Tests;

class BaseClass {
	public function __construct() {}
}

class EmptyExtendingClass extends BaseClass {

}

class BaseClassOldStyle {
	public function BaseClassOldStyle($a, $b) {}
}

class EmptyExtendingOldStyleClass extends BaseClassOldStyle {

}
