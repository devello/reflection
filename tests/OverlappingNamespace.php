<?php

namespace {
	function overlappingExample() { return 'global function'; }
	class overlappingExample {}
}

namespace overlappingExample {
	function overlappingExample() { return 'namespaced function'; }
}
