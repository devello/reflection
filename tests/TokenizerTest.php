<?php
/*
 Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of Tomasz Cichecki nor the  names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Devello\Reflection\Tests;

use Devello\Tokenizer;

class TokenizerTest extends \PHPUnit_Framework_TestCase {
	private $eventRaised = array();
	private $called;
	private $tokenizer;
	
	public function setUp() {
		$this->called = 0;
		$this->eventRaised = array();
		
		$this->tokenizer = new Tokenizer(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Examples.php');
	}
	
	public function testScanningFile_File_FileStoredInCache() {
		$tokenizer = new Tokenizer(__FILE__);
		$tokenizer->scan();
	}
	
	public function testScanningFile_ParseExampleFile_CallsObserversUponRunningIntoNamespaceToken() {
		$this->tokenizer->on(T_NAMESPACE, $this);
		$this->tokenizer->scan();
		
		$this->assertTrue(!empty($this->called));
	}
	
	public function testScanningFile_RegisterForClass_CallcountMatchesNumberOfClasses() {
		$this->tokenizer->on(T_CLASS, $this);
		$this->tokenizer->scan();
		
		$this->assertEquals(13, $this->called);
	}
	
	public function testScanningFile_ParseEmptyFile_NoEventsGenerated() {
		$tokenizer = new Tokenizer(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Empty.php');
		$tokenizer->on(T_NAMESPACE , $this);
		$tokenizer->scan();
		
		$this->assertTrue(empty($this->called));
	}
	
	public function testScanningFile_NamespaceEvent_EventWithNamespaceNameIsGenerated() {
		$this->tokenizer->on(T_NAMESPACE, $this);
		$this->tokenizer->scan();
		
		$this->assertContains('\\Devello\\Reflection\\Tests', $this->eventRaised[0]->name);
	}
	
	public function testScanningFile_ClassEvent_EventWithClassnameIsGenerated() {
		$this->tokenizer->on(T_CLASS, $this);
		$this->tokenizer->scan();
		
		$this->assertContains('\\Devello\\Reflection\\Tests\\Example', $this->eventRaised[0]->name);
	}
	
	public function testScanningFile_MethodEvent_EventWithMethodNameIsGenerated() {
		$this->tokenizer->on(T_FUNCTION, $this);
		$this->tokenizer->scan();
		
		$this->assertEquals('\\Devello\\Reflection\\Tests\\Example::newInstanceWithoutConstructor', $this->eventRaised[0]->name);
	}
	
	public function testScanningFile_MethodEvent_EventBearsNumbersOfStartAndEndTokens() {
		$this->tokenizer->on(T_FUNCTION, $this);
		$this->tokenizer->scan();
		
		$this->assertEquals(21, $this->eventRaised[0]->start);
		$this->assertEquals(283, $this->eventRaised[0]->end);
	}
	
	public function testScanningFile_MethodEvent_NextMethodsGenerated() {
		$this->tokenizer->on(T_FUNCTION, $this);
		$this->tokenizer->scan();
		
		$this->assertEquals('\\Devello\\Reflection\\Tests\\Example::hasConstant', $this->eventRaised[1]->name);
	}
	
	public function testScanningFile_ClassEvent_NextClassGenerated() {
		$this->tokenizer->on(T_CLASS, $this);
		$this->tokenizer->scan();
		
		$this->assertContains('\\Devello\\Reflection\\Tests\\Example3', $this->eventRaised[1]->name);
	}
	
	public function testScanningFile_FunctionEvent_EventWithFunctionNameIsGenerated() {
		$this->tokenizer->on(T_FUNCTION, $this);
		$this->tokenizer->scan();
		
		$this->assertEquals('\\Devello\\Reflection\\Tests\\method1', $this->eventRaised[38]->name);
	}
	
	public function testScanningFile_NamespaceEvent_InlinedNamespacesDetected() {
		$this->tokenizer = new Tokenizer(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NamespacedExamples.php');
		
		$this->tokenizer->on(T_NAMESPACE, $this);
		$this->tokenizer->scan();
		$this->assertEquals(5, $this->called);
	}
	
	public function update($event) {
		$this->called++;
		$this->eventRaised[] = $event;
	}
}
