<?php
/*
 Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
* Neither the name of Tomasz Cichecki nor the  names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace Devello\Reflection\Tests;
/** @formatter:off */
abstract class Example {

	public function newInstanceWithoutConstructor() {

		$properties = $this->getProperties();
		$defaults = $this->getDefaultProperties();

		$serialized = "O:" . strlen($this->getName());
		$serialized .= ":\"{$this->getName()}\":" . count($properties) . ':{';

		foreach ($properties as $property) {
			$name = $property->getName();
			if ($property->isProtected()) {
				$name = chr(0) . '*' . chr(0) . $name;
			} elseif ($property->isPrivate()) {
				$name = chr(0) . $this->getName() . chr(0) . $name;
			}
			$serialized .= serialize($name);

			$default = null;
			if (array_key_exists($property->getName(), $defaults)) {
				$default = $defaults[$property->getName()];
			}
			$serialized .= serialize($default);
		}
		$serialized .= "}";
		return unserialize($serialized);
	}

	public function hasConstant($arg0) {
		return $this->_instance->hasConstant($arg0);
	}

	public function oneLineMethod() {} public function theSameLine() {}

	public
function veryBadFormated(

	) {
		return function () {};
	}

	function withoutAnyModifiers() {}
	abstract function anAbstractMethod();function nonAbstract() {}

	public function curlyVariables() {
		$x = 50;
		print "{$x}";
		print "${x}";
	}
}

abstract class Example3 {
	public abstract function method1();
	public abstract function method0();
	public function method2() { return 0; }
}

class Example4 {
	public function __construct() {
		$this->_world = "World";
	}
	public static function helloWorld() {
		return "Hello World!";
	}

	public function hello() {
		return "Hello {$this->_world}!";
	}

	public function greetThem($who=NULL) {
		return "Hello " . $who . "!";
	}

	public static function greet($who) {
		return "Hello " . $who . "!";
	}

	protected function saluteToTheFriends() {
		return "Hello World!";
	}

	protected function saluteToTheCommerades($theCommerades) {
		return "Hello " . $theCommerades . "!";
	}

	private static function salutToTheWorld() {
		return "Hello World!";
	}

	private function salutTo($theWorld) {
		return "Hello " . $theWorld . "!";
	}

}

class Example5 {
	private function &returnReference(&$b) {
		return $b;
	}

	public function &returnByReference(&$b) {
		return $b;
	}
	
	public function implode(array $args) {
		return implode(", ", $args);
	}
}

final class Example6Final {
	public function &f(&$ref) {
		return $ref;
	}
	
	public static function g() {}
}

class ExampleEmptyClass {

}

class ExampleBadFormatting {} class AnotherExampleOfBadFormatting {

}

class ExampleWithProtectedMethods {

	protected function method_protected() { }

	public function method_public_that_calls_protected_method() {
		return $this->method_protected();
	}

	public function method_public() {
		return $this->method_protected();
	}

	public function &methodWithReference(&$a) {
		return $a;
	}
}

interface DummyInterface {

}

class Example1 extends ExampleBadFormatting {}

class Example2 implements DummyInterface {}
class Example_4 implements DummyInterface {private $_p0; public $_p1; protected $_p2;}

class ExampleSignatures {
	public function method1($params) {}
	public function &method2(&$params) {}
	public function method3($params=NULL) {}
	private function &method4($a, $b, $c, $d) {}
	protected function method5(\stdClass $x) {}
	protected function method6(\stdClass &$x=NULL){}
	public function method7(\Devello\Reflection\Tests\ReflectionMethodTest $param) {}
}

function method1($params) {}
function &method2(&$params) {}
function method3($params=NULL) {}
function &method4($a, $b, $c, $d) {}
function method5(\stdClass $x) {}
function method6(\stdClass &$x=NULL){}
function method7(\Devello\Reflection\Tests\ReflectionFunctionTest $param){}

/** @formatter:on */