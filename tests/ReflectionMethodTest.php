<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Devello\Reflection\Tests;

use Devello\ReflectionClass, Devello\ReflectionMethod;
use PHPUnit_Framework_TestCase;

include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NoNamespace.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NamespacedExamples.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Examples.php';

class ReflectionMethodTest extends PHPUnit_Framework_TestCase
{
	/** @var ReflectionMethod */
	protected $_reflector;
	
	public function setUp() {
		$this->_reflector = ReflectionClass::getReflection('Devello\\Reflection\\Tests\\Example');
	}
	
	public function testGetSourceCode_WellFormedSourceCode_FullSourceCodeIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('hasConstant');

		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals(trim("public function hasConstant(\$arg0) {
		return \$this->_instance->hasConstant(\$arg0);
	}"), $source);
	}

	public function testGetSourceCode_WellFormedSourceCodeWithControlStructures_FullSourceCodeIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('newInstanceWithoutConstructor');

		$source = trim($methodReflection->getSourceCode());
		$this->assertEquals(trim("
	public function newInstanceWithoutConstructor() {

		\$properties = \$this->getProperties();
		\$defaults = \$this->getDefaultProperties();

		\$serialized = \"O:\" . strlen(\$this->getName());
		\$serialized .= \":\\\"{\$this->getName()}\\\":\" . count(\$properties) . ':{';

		foreach (\$properties as \$property) {
			\$name = \$property->getName();
			if (\$property->isProtected()) {
				\$name = chr(0) . '*' . chr(0) . \$name;
			} elseif (\$property->isPrivate()) {
				\$name = chr(0) . \$this->getName() . chr(0) . \$name;
			}
			\$serialized .= serialize(\$name);

			\$default = null;
			if (array_key_exists(\$property->getName(), \$defaults)) {
				\$default = \$defaults[\$property->getName()];
			}
			\$serialized .= serialize(\$default);
		}
		\$serialized .= \"}\";
		return unserialize(\$serialized);
	}"), $source);
	}

	public function testGetSourceCode_BadlyFormedSourceCode_SourceCodeIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('oneLineMethod');

		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('public function oneLineMethod() {}', $source);
	}

	public function testGetSourceCode_MultiLineBadlyFormedSourceCode_SourceCodeIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('veryBadFormated');

		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('public
function veryBadFormated(

	) {
		return function () {};
	}', $source);
	}

	public function testGetSourceCode_ImplicitPublicModifier_SourceCodeWithoutPublicModifierIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('withoutAnyModifiers');

		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('function withoutAnyModifiers() {}', $source);
	}

	public function testGetSourceCode_AbstractMethod_TheEmptyBodyIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('anAbstractMethod');

		$source = trim($methodReflection->getSourceCode());
		$this->assertEquals('abstract function anAbstractMethod();', $source);
	}

	public function testGetSourceCode_CurlyBracketVariableSyntax_TheMethodBodyIsRetrieved() {
		$methodReflection = $this->_reflector->getMethod('curlyVariables');
		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('public function curlyVariables() {
		$x = 50;
		print "{$x}";
		print "${x}";
	}', $source);
	}

	public function testGetSourceCode_InlineNamespacesWithComplexSyntax_TheMethodIsRetrieved() {
		$methodReflection = \Devello\ReflectionMethod::getReflection('\\Devello\\Tests\\name4\\FooBar','method');
		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('public function method()
	{
		// TODO: Implement method() method.
	}', $source);
	}

	public function testGetSourceCode_AnAbstractMethodComesFirst_TheMethodRequestedIsRetrieved() {
		$methodReflection = \Devello\ReflectionMethod::getReflection('Devello\\Reflection\\Tests\\Example3', 'method2');
		$source = trim($methodReflection->getSourceCode());

		$this->assertEquals('public function method2() { return 0; }', $source);
	}


	public function testEvaldFilename_CallingGetFilenameOnDifferentEvaldMethod_SameFilenameIsReturned() {
		$type = $this->getMock('stdClass', array('a', 'b'));
		$className =get_class($type);

		$reflection1 = ReflectionMethod::getReflection($type, 'a');
		$reflection2 = ReflectionMethod::getReflection($type, 'b');
		$this->assertEquals($reflection1->getFileName(), $reflection2->getFileName());
	}

	public function testEvaldFilename_CallingGetFilenameOnEvaldMethod_SameFilenameAsDeclaringClassIsReturned() {
		$type = $this->getMock('stdClass', array('a', 'b'));

		$class_reflection = ReflectionClass::getReflection($type);

		$method_reflection = $class_reflection->getMethod('a');

		$this->assertEquals($method_reflection->getFileName(), $class_reflection->getFileName());
	}

	public function testEvaldFilename_CheckingIfSourceIsEvald_ReflectionReturnsTrue() {
		$type = $this->getMock('stdClass', array('a', 'b'));

		$class_reflection = ReflectionClass::getReflection($type);

		$method_reflection = $class_reflection->getMethod('a');

		$is_evaled = $method_reflection->isEvaled();

		$this->assertTrue($is_evaled);
	}

	public function testEvaldFilename_CheckingIfSourceIsEvaldSourceCodeExists_ReflectionReturnFalse() {

		$class_reflection = ReflectionClass::getReflection($this);

		list(, $method) = explode("::", __METHOD__);
		$method_reflection = $class_reflection->getMethod($method);

		$is_evaled = $method_reflection->isEvaled();

		$this->assertFalse($is_evaled);
	}

	public function testBackPort_GetClosure_CanGetClosureOfStaticMethod() {
		$reflection = ReflectionMethod::getReflection('Devello\\Reflection\\Tests\\Example4::helloWorld');

		$closure = $reflection->getClosure();

		$return_value = call_user_func($closure);

		$this->assertEquals('Hello World!', $return_value);
	}

	public function testBackPort_GetClosure_CanGetClosureOfAnInstanceMethod() {
		$example = new Example4();
		$reflection = ReflectionMethod::getReflection($example, 'hello');
		$closure = $reflection->getClosure($example);

		$return_value = call_user_func($closure);

		$this->assertEquals('Hello World!', $return_value);
	}

	public function testBackPort_GetClosure_CanGetClosureOfAnInsatnceMethodWithParameters() {
		$example = new Example4();

		$reflection = ReflectionMethod::getReflection($example, 'greetThem');
		$closure = $reflection->getClosure($example);
		$return_value = call_user_func($closure, 'World');

		$this->assertEquals('Hello World!', $return_value);
	}

	private function _getClosure($name, $example='Devello\\Reflection\\Tests\\Example4') {
		$obj = new $example();

		$reflection = ReflectionMethod::getReflection($obj, $name);
		$closure = $reflection->getClosure($obj);
		return $closure;
	}

	public function testBackPort_GetClosure_CanGetCallableClosureOfPrivateInstanceMethods() {
		$closure = $this->_getClosure('salutTo');

		$result_value = call_user_func($closure, 'World');

		$this->assertEquals('Hello World!', $result_value);
	}

	public function testBackPort_GetClosure_ClosureCanPassAndReturnByReference() {
		$closure = $this->_getClosure('returnByReference', 'Devello\\Reflection\\Tests\\Example5');

		$a = 5;
		$b = &$closure($a);

		$b++;

		$this->assertEquals($b, $a);
	}

	public function testOptionalParameter_DefaultsToNull_NullIsOptputedNotString() {
		$obj = new Example4;
		$reflection = ReflectionMethod::getReflection($obj, 'greetThem');


		$this->assertNotContains("'NULL'", $reflection->getSourceCode());
	}

	public function testMethodSignature_ArrayTypehintedMethod_ArrayTypehintIsIncludedInTheSignature() {
		$obj = new Example5;
		$reflection = ReflectionMethod::getReflection($obj, 'implode');
	
		$signature = $reflection->getSourceCode();
	
		$this->assertContains('public function implode(array $args)', $signature);
	
	}
	
	public function testGetClosure_FinalReturningRef_RefIsNotLost() {
		$obj = new Example6Final();
		$reflection = ReflectionMethod::getReflection($obj, 'f');

		$closure = $reflection->getClosure($obj);
		
		$a = 5;
		$b =&$closure($a);
		$b =6;
		
		$this->assertEquals($a, $b);
		
	}
	
	public function testGetClosure_ClosureNameFromMethodReflection_NameIsSameLikeClosuredMethod() {
		$obj = new Example6Final();
		$reflection = ReflectionMethod::getReflection($obj, 'f');
		
		$closure = $reflection->getClosure($obj);
		$closureReflection = new \Devello\ReflectionFunction($closure);
		
		$this->assertEquals('f', $closureReflection->getName());
		
	}
	
	public function testGetClosureThis_InstanceMethod_InstanceBoundToClosuresThisIsReturned() {
		$obj = new Example6Final();
		$reflection = ReflectionMethod::getReflection($obj, 'f');
		$closure = $reflection->getClosure($obj);
		$closureReflection = new \Devello\ReflectionFunction($closure);
		$closureThis = $closureReflection->getClosureThis();
		
		$this->assertEquals($obj, $closureThis);
	}
	
	public function testGetClosureThis_StaticMethod_StaticMethodsAreNotBoundToAnyInstanceNullIsReturned() {
		$obj = new Example6Final();
		$reflection = ReflectionMethod::getReflection($obj, 'g');
		$closure = $reflection->getClosure();
		$closureReflection = new \Devello\ReflectionFunction($closure);
		$closureThis = $closureReflection->getClosureThis();
		
		$this->assertNull($closureThis);
	}

	public function testGetClosure_GetClosureScopeClass_ReflectionClassOfTheClosuredMethodIsReturned() {
		$obj = new Example6Final();
		$reflection = ReflectionMethod::getReflection($obj, 'g');
		$closure = $reflection->getClosure();
		$closureReflection = new \Devello\ReflectionFunction($closure);
		
		$closureScopeClass = $closureReflection->getClosureScopeClass();
		
		$this->assertInstanceOf('ReflectionClass', $closureScopeClass);
		$this->assertEquals(__NAMESPACE__ . '\\' . 'Example6Final', $closureScopeClass->getName());
		
	}
	
	public function testIsMagic_Constructor_IsMagicShouldReturnTrue() {
		$reflection = ReflectionMethod::getReflection(__NAMESPACE__ . '\\Example4::__construct');
		
		$this->assertTrue($reflection->isMagic());
	}
	
	public function testIsMagic_CommonMethod_IsMagicShouldReturnFalse() {
		$reflection = ReflectionMethod::getReflection(__NAMESPACE__ . '\\Example4::helloWorld');
		
		$this->assertFalse($reflection->isMagic());
	}
	
	public function testGetSourceCode_EvaldInterfaceMethod_SourceWithoutBodyIsRetrieved() {
		$code = 'interface TestInterface { public function exampleMethod(); }';
		eval($code);
		
		$reflection = new ReflectionMethod('TestInterface::exampleMethod');
		$source = $reflection->getSourceCode();
		
		$this->assertEquals('public function exampleMethod();', $source);
	}
	
	public function testGetSourceCode_EvaldNamespacedInterfaceMethod_SourceCodeIsRetrieved() {
		$code = '
			namespace Devello\\ReflectionMethodTest\\Evald;
			interface TestInterface2 { public function &exampleMethod(); }
		';
		eval($code);
		
		
		$reflection = new ReflectionMethod('\\Devello\\ReflectionMethodTest\\Evald\\TestInterface2::exampleMethod');
		
		$source = $reflection->getSourceCode();
		$this->assertEquals('public function &exampleMethod();', $source);
	}
	
	public function testGetFilename_EvaldNamespacedInterfaceMethod_MethodClassInterfaceShareTheSameFilename() {
		$code = '
			namespace Devello\\ReflectionMethodTest\\Evald;
			interface TestInterface { public function &exampleMethod(); }
		';
		eval($code);
		
		$reflection = new ReflectionMethod('\\Devello\\ReflectionMethodTest\\Evald\\TestInterface::exampleMethod');

		$reflectedFilename = $reflection->getFileName();
		
		$declaringClassReflectedFilename = $reflection->getDeclaringClass()->getFileName();
		$this->assertEquals($reflectedFilename, $declaringClassReflectedFilename);
	}
	/**
	 * 
	 * @dataProvider signatures
	 */
	public function testMethodSignature_VariousParameters_ReturnsCorrectMethodSignature($method, $expected) {
		$reflection = new ReflectionMethod(__NAMESPACE__ . '\\' . $method);
		
		$signature = $reflection->methodSignature();
		
		$this->assertEquals($expected, $signature);
	}
	
	public function testGetSourceCode_ClassNoNamespace_MethodOfClassRetrieved() {
		$reflection = new ReflectionMethod('NoNamespace_Example_Extended::abc');
		$source = $reflection->getSourceCode();
	}
	
	public function signatures() {
		return array(
			array('ExampleSignatures::method1', 'public function method1($params)'),
			array('ExampleSignatures::method2', 'public function &method2(&$params)'),	
			array('ExampleSignatures::method3', 'public function method3($params=NULL)'),
			array('ExampleSignatures::method4', 'private function &method4($a, $b, $c, $d)'),
			array('ExampleSignatures::method5', 'protected function method5(\\stdClass $x)'),
			array('ExampleSignatures::method6', 'protected function method6(\\stdClass &$x=NULL)'),
			array('ExampleSignatures::method7', 'public function method7(\\Devello\\Reflection\\Tests\\ReflectionMethodTest $param)')
		);
	}
}
