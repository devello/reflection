<?php
/*
Copyright (c) 2013-2014, Tomasz Cichecki
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Tomasz Cichecki nor the  names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
namespace Devello\Reflection\Tests;

use Devello\ReflectionFunction, PHPUnit_Framework_TestCase;
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Examples.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'NamespacedExamples.php';
include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'OverlappingNamespace.php';

class ReflectionFunctionTest extends PHPUnit_Framework_TestCase {
	public function testGetSourceCode_ExtractingFunctionCode_CodeIsExtracted() {
		$reflection = ReflectionFunction::getReflection('\\Devello\\Reflection\\Tests\\exampleFunction');
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertEquals('function exampleFunction () {}', $sourceCode);
	}
	
	public function testGetSourceCode_ExtractingNamespacedFunctionCode_CodeIsExtracted() {
		$reflection = ReflectionFunction::getReflection('Devello\Tests\name3\\method');
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertEquals('function method() { return 1; }', $sourceCode);
	}
	
	public function testGetClosure_CallingFunctionAsClosure_CallIsSuccessful() {
		$reflection = ReflectionFunction::getReflection('Devello\Tests\name3\\method');
		$closure = $reflection->getClosure();
		
		$result = $closure();
		
		$this->assertEquals(1, $result);
	}
	
	public function testGetSourceCode_EvaldFunctionCode_StubbedCodeIsReturned() {
		$name = uniqid('evaled_');
		eval(sprintf("function %s() {}", $name));
		
		$reflection = ReflectionFunction::getReflection($name);
		
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertContains($name, $sourceCode);
	}
	
	public function testGetSourceCode_EvaldFunctionCodeInNamespace_StubbedCodeIsReturned() {
		$name = uniqid('evaled_');
		eval(sprintf("
				namespace A\\SampleNamespace;
				function %s() {}
		", $name));
		
		$reflection = ReflectionFunction::getReflection('A\\SampleNamespace\\' . $name);
		
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertContains($name, $sourceCode);
	}
	
	public function testGetSourceCode_Closure_ClosureCodeIsReturned() {
		
		$closure = function () {
			return 'This is a closure';
		};
		
		$reflection = ReflectionFunction::getReflection($closure);
		
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertContains('This is a closure', $sourceCode);
		$this->assertEquals("function () {
			return 'This is a closure';
		};", $sourceCode);
	}
	
	public function testGetSourceCode_ClosureReturnsReference_ClosureCodeIsReturned() {
		$closure = function &() {
			$ref = 'This is a closure';
		    return $ref;
		};
		
		$reflection = ReflectionFunction::getReflection($closure);
		
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertContains('&()', $sourceCode);
		
	}
	
	public function testClosureName_GetName_ReturnsNameBasedOnPositionInTheFilesystem() {
		
		$lineStart = __LINE__; $closure = function () {
			return 'This is a closure';
		}; $lineEnd = __LINE__;
		
		$reflection = ReflectionFunction::getReflection($closure);
		
		$closureName = $reflection->getName();
		$expected = "Devello\Reflection\Tests\{closure[$lineStart:$lineEnd]}";
		$this->assertEquals($expected, $closureName);
	}
	
	public function testGetSourceCode_FunctionNameSameAsClassName_FunctionSourceIsReturned() {
		$reflection = ReflectionFunction::getReflection('Devello\\Reflection\\Tests\\exampleFunction2');
		
		$expected = "function exampleFunction2 () { return 'Function sourcecode'; }";
		
		$this->assertEquals($expected, $reflection->getSourceCode());
	}
	
	public function testGetClosure_CallCreatedClosure_ShouldBehaveLikeOriginalFunction() {
		$reflection = ReflectionFunction::getReflection('Devello\\Reflection\\Tests\\exampleFunction2');
		$closure = $reflection->getClosure();
		
		$expected = 'Function sourcecode';
		$actual = $closure();
		
		$this->assertEquals($expected, $actual);
	}
	
	public function testGetClosure_CallCreatingClosureOfRefsPassingFunction_BehaveLikeOriginalFunction()
	{
		$reflection = ReflectionFunction::getReflection('Devello\\Reflection\\Tests\\function_function');
		$closure = $reflection->getClosure();
		
		$a = 5;
		$b = &$closure($a);
		
		$a++;
		
		$this->assertEquals($a, $b);
	}
	
	
	public function testGetClosure_GeneratedClosure_GeneratedClosureReflectedNameSameAsOfReflectedFunction() {
		
		$reflection = ReflectionFunction::getReflection('Devello\\Reflection\\Tests\\function_function');
		$closure = $reflection->getClosure();
		
		$reflection = new ReflectionFunction($closure);
		$this->assertEquals('Devello\\Reflection\\Tests\\function_function', $reflection->getName());
	}
	
	public function testGetClosure_GetClosureThis_FunctionAreNotBoundToAnyInstance() {
		$reflection = ReflectionFunction::getReflection('Devello\\Reflection\\Tests\\function_function');
		$closure = $reflection->getClosure();
		
		$reflection = new ReflectionFunction($closure);
		$closureThis = $reflection->getClosureThis();
		$this->assertNull($closureThis);
	}
	
	/**
	 * @dataProvider names
	 * @param unknown $name
	 */
	public function testGetSourceCode_CallingFunctionEvalFromCentralLine_StubbedCodeIsReturned($name) {
		$this->_myEval(sprintf("
				namespace A\\SampleNamespace;
				function %s() {}
		", $name));
		
		$reflection = ReflectionFunction::getReflection('A\\SampleNamespace\\' . $name);
		$source = $reflection->getSourceCode();
		
		$this->assertContains($name, $source);
	}
	
	/**
	 * @dataProvider signatures
	 */
	public function testSignature_VariousParameters_ReturnsCorrectMethodSignature($function, $expected) {
		$reflection = new ReflectionFunction(__NAMESPACE__ . '\\' . $function);
		
		$signature = $reflection->methodSignature();
		
		$this->assertEquals($expected, $signature);
	}
	
	public function testGetSourceCode_InlineNamespaceOverlapsFunctionName_SourceCodeIsExtracted() {
		$reflection = new ReflectionFunction('\\overlappingExample');
		
		$sourceCode = $reflection->getSourceCode();
		
		$this->assertEquals("function overlappingExample() { return 'global function'; }", $sourceCode);
	}
	
	protected function _myEval($code) {
	    return eval($code);
	}
	
	public function names() {
		return array(
			array(uniqid('evaled_1')),
			array(uniqid('evaled_2'))	
		);
	}
	
	public function signatures() {
	    return array(
	            array('method1', 'function method1($params)'),
	            array('method2', 'function &method2(&$params)'),
	            array('method3', 'function method3($params=NULL)'),
	            array('method4', 'function &method4($a, $b, $c, $d)'),
	            array('method5', 'function method5(\\stdClass $x)'),
	            array('method6', 'function method6(\\stdClass &$x=NULL)'),
	            array('method7', 'function method7(\\Devello\\Reflection\\Tests\\ReflectionFunctionTest $param)')
	    );
	}
} 
